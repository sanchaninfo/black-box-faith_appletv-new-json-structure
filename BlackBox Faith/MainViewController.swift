//
//  ASMainViewController.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//
import UIKit


protocol CarousalSelectDelegate: class
{
    func carousalSelected(carousalName:String,carousalDetailDict:[[String:Any]],userid:String,deviceid:String)
    func collectionItemSelected(forUrl:String)
}

class MainViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,myListdelegate,UICollectionViewDelegate,UICollectionViewDataSource{
    
    var CarousalName = NSMutableArray()
    var CategoryCarousalName = NSMutableArray()
    var searchCollectionList = NSMutableArray()
    var deviceId,userId,uuid : String!
    var myList = [[String:Any]]()
    var CarousalData = [[String:Any]]()
    var MenuCarousalData = [[String:Any]]()
    var recentList = [[String:Any]]()
    weak var delegate:CarousalSelectDelegate?
    var storeProdData = [[String:Any]]()
    var PhotoDict = [[String:Any]]()
    var FeaturedDict = [[String:Any]]()
    var DonateDict = [[String:Any]]()
    var mylistSection = Int()
    var recentSection = Int()
    
    @IBOutlet var likeLbl: UILabel!
    
    @IBOutlet var dislikeLbl: UILabel!
    @IBOutlet weak var MainImage1: UIImageView!
    @IBOutlet var listView:UITableView!
    @IBOutlet weak var MainImage: UIImageView!
    @IBOutlet weak var AssestName: UILabel!
    @IBOutlet weak var ReleaseDate: UILabel!
    @IBOutlet weak var TimeLbl : UILabel!
    @IBOutlet weak var Description: UITextView!
    @IBOutlet var staticLbl: UILabel!
    @IBOutlet var lbl2:UILabel!
    @IBOutlet var collectionView: UICollectionViewCell!
    var activityView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
//        listView.contentSize = CGSize(width: listView.frame.size.width, height: CGFloat(409))
        activityView = ActivityView.init(frame: self.view.frame)
        self.view.addSubview(activityView)
        
        (self.view.viewWithTag(1))?.isHidden = true
        
        getFeatured()
        if userId != ""
        {
            getFetchMyList(withurl: kFetchMyListUrl)
            getFetchRecentList(withurl: kFetchRecentUrl)
        }
     //   StoreDetails()
       // getPhotos()
       // getDonatedata()
        getcarosuel(withurl: kCarouselUrl)
        self.CarousalName.add("Menu")
        if FeaturedDict.count != 0
        {
            self.CarousalName.add("SpotLight")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
         getaccountDetails()
//        getFetchMyList(withurl: kFetchMyListUrl)
//        getFetchRecentList(withurl: kRecentlyWatchedUrl)
    }
    func getaccountDetails()
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
        
        ApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                let ispaired = (dict.value(forKey: "uuid_exist") as! Bool)
                if ispaired
                {
                    self.getFetchMyList(withurl: kFetchMyListUrl)
                    self.getFetchRecentList(withurl: kFetchRecentUrl)
                }
                else
                {
                    self.recentList.removeAll()
                    self.myList.removeAll()
                    DispatchQueue.main.async {
                        self.CarousalName.remove("Recently Watched")
                        self.CarousalName.remove("My List")
                        self.listView.reloadData()
                    }
                    
                }
                
            }
            else
            {
                
            }
        }
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.CarousalName.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = CollectionContentCell()
        // For Menu Collection view
        MenuCarousalData = [["metadata": ["carouselId":"Menu","landscape_url_500x281":UIImage.init(named: "searchicon")!,kIsmenu: true,"banner_image_1300x650":"https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/ebef793c-53f9-40a7-83f2-813fb2fb2b2f.png"]],["metadata": ["carouselId":"Menu","landscape_url_500x281":UIImage.init(named: "categories_small")!,kIsmenu: true,"banner_image_1300x650":"https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/ebef793c-53f9-40a7-83f2-813fb2fb2b2f.png"]],["metadata": ["carouselId":"Menu","landscape_url_500x281":UIImage.init(named: "settings_small")!,kIsmenu: true,"banner_image_1300x650":"https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/ebef793c-53f9-40a7-83f2-813fb2fb2b2f.png"]]]
        
        if indexPath.section == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! CollectionContentCell
            cell.carousalSelected(carousalName: "Menu", carousalDetailDict: MenuCarousalData,userid:userId,deviceid: deviceId,uuid:uuid)
            
        }
         else
        {
            if ((CarousalName[indexPath.section] as! String) == "SpotLight")
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "FeaturedCell", for: indexPath) as! CollectionContentCell
                cell.carousalSelected(carousalName: "SpotLight", carousalDetailDict: FeaturedDict, userid: userId, deviceid: deviceId, uuid: uuid)
            }
            else if ((CarousalName[indexPath.section] as! String) == "My List")
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "myListCell", for: indexPath) as! CollectionContentCell
                cell.carousalSelected(carousalName: "My List", carousalDetailDict: myList, userid: userId, deviceid: deviceId,uuid: uuid)
            }
            else if ((CarousalName[indexPath.section] as! String) == "Recently Watched")
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "recentCell", for: indexPath) as! CollectionContentCell
                cell.carousalSelected(carousalName: "Recently Watched", carousalDetailDict: recentList, userid: userId, deviceid: deviceId,uuid:uuid)
            }
            else if (CarousalName[indexPath.section] as! String) == "Photos"
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "carousalCell", for: indexPath) as! CollectionContentCell
                cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: PhotoDict , userid: userId, deviceid: deviceId, uuid: uuid)
            }
                
            else if (CarousalName[indexPath.section] as! String == "Store")
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "carousalCell", for: indexPath) as! CollectionContentCell
                cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: storeProdData , userid: userId, deviceid: deviceId, uuid: uuid)
            }
            else
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "carousalCell", for: indexPath) as! CollectionContentCell
                cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: CarousalData,userid: userId,deviceid: deviceId,uuid:uuid)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view1 = UIView(frame: CGRect(x: 45, y: 0, width: tableView.frame.width, height: 34))
        let header = UILabel(frame: CGRect(x: 45, y:0 , width: tableView.frame.width, height: 34))
        header.tag = 100
        header.font = UIFont.init(name: "Helvetica-Bold", size: 34)
        header.textAlignment = .left
        header.text = CarousalName[section] as? String
        header.textColor = UIColor.white
        view1.addSubview(header)
        return view1
    }
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(12) as! UILabel).text = String(indexPath.row)
        return cell
    }

//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        for cell in self.listView.visibleCells {
//            let indexPath = self.listView.indexPath(for: cell)!
//            let attributes = CGRect(x: listView.frame.origin.x, y: listView.frame.origin.y, width: listView.frame.size.width, height: 250)
//         //   let attributes = self.listView.layoutAttributesForItem(at: indexPath)!
//            let cellRect = attributes
//            let cellFrameInSuperview = self.listView.convert(attributes, to: self.listView.superview)
//            let centerOffset = cellFrameInSuperview.origin.x - self.listView.contentInset.left
//            cell.updateConstraints()
//          //  cell.updateConstraints()
//         //   cell.updateWithOffset(centerOffset)
//        }
//    }
    func getcarosuel(withurl:String)
    {
        var parameters = [String:[String:AnyObject]]()
//        var user_ID = String()
//        if userId == ""
//        {
//            user_ID = "guest_id"
//        }
//        else
//        {
//            user_ID = userId
//        }
        parameters = ["getCarousels":["id": "guest_id" as AnyObject , "userType":kUserType as AnyObject]]
        print(parameters)
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error,isDone) in
            if error == nil
            {
                let post = responseDict as! [[String:Any]]
                for dict in post
                {
                    let carousalObj = Carousal().initWithName(name: dict["carousel_name"] as! String, mobile: dict["mobile"] as! String, order: String(describing: dict["order"]), site: dict["site"] as! String, status: dict["status"] as! String, tv: dict["tv"] as! String)
                    if (carousalObj.carousel_name == "Photos") || (carousalObj.carousel_name == "Store")
                    {
                        print("\(carousalObj.carousel_name) is Skipped")
                    }
                    else
                    {
                        self.CarousalName.add(carousalObj.carousel_name)
                        self.CategoryCarousalName.add(carousalObj.carousel_name)
                    }
                }
             
                for i in 0..<self.CarousalName.count
                {
                    if ((self.CarousalName[i] as! String) == "Menu") || ((self.CarousalName[i] as! String) == "My List") || ((self.CarousalName[i] as! String) == "Recently Watched") || ((self.CarousalName[i] as! String) == "Store") || ((self.CarousalName[i] as! String) == "Photos") || ((self.CarousalName[i] as! String) == "SpotLight")
                    {
                        print(self.CarousalName[i])
                    }
                    else
                    {
                        self.getCarousalDetails(name: kCarouselDataUrl,carousal:self.CarousalName[i] as! String)
                    }
            }
                
            }
            else
            {
                let alertview = UIAlertController(title: "Server Down", message: "Due to some Techincal Problem Server Down" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    // exit(0)
                })
                alertview.addAction(defaultAction)
                self.present(alertview, animated: true, completion:nil)
            }
        }
    }
    
    func getCarousalDetails(name:String,carousal:String)
    {
        var parameters = [String:[String:AnyObject]]()
//        var user_ID = String()
//        if userId == ""
//        {
//            user_ID = "guest_id"
//        }
//        else
//        {
//            user_ID = userId
//        }
        parameters =  ["getCarouselData":["name":(carousal as AnyObject),"id":"guest_id" as AnyObject, "userType":kUserType as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: name, parameters: parameters){(responseDict, error, isDone) in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                let arr = JSON[carousal]
             
                for dict in arr as! [[String:Any]]
                {
                    let dicton = dict as NSDictionary
                    self.CarousalData.append(dicton as! [String : Any])
                    self.searchCollectionList.add(dicton)
                }
                DispatchQueue.main.async
                    {
                        UIView.transition(with: self.listView, duration: 1.0, options: .transitionCrossDissolve, animations: {self.listView.reloadData()}, completion: nil)
                    }
               
            }
            else
            {
                    let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                        UIAlertAction in
                    let _ = self.navigationController?.popToRootViewController(animated: true)
                    })
                    alertview.addAction(defaultAction)
                    self.present(alertview, animated: true, completion: nil)
             
            }
        }
        activityView.removeFromSuperview()
    }
    
    func getFetchMyList(withurl:String)
    {
        var parameters = [String:[String:AnyObject]]()
        parameters = ["fetchMyList":[ "userId": userId as AnyObject , "id":deviceId as AnyObject, "userType":kUserType as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error, isDone) in
            if error ==  nil
            {
                let JSON = responseDict as! NSDictionary
                let arr = JSON["myList"] as! NSArray
                var containMylist = String()
               
                self.myList.removeAll()
                
                if arr.count == 0
                {
                    self.CarousalName.remove("My List")
                }
                for dict in arr as! [[String:Any]]
                {
                    let dicton = dict as NSDictionary
                    self.myList.insert(dicton as! [String:Any], at: 0)
                   
                }
                DispatchQueue.main.async
                    {
                        if self.CarousalName.contains("My List")
                        {
                            self.mylistSection = self.CarousalName.index(of: "My List")
                            containMylist = "yes"
                        }
                        else
                        {
                            if  !(self.myList.isEmpty)
                            {
                                if self.CarousalName.contains("SpotLight")
                                {
                                    self.CarousalName.insert("My List", at: 2)
                                    self.mylistSection = 2
                                }
                                else
                                {
                                    self.CarousalName.insert("My List", at: 1)
                                    self.mylistSection = 1
                                }
                            }
                            containMylist = "no"
                      
                        }
                        if kisMyListUpdated == "MyListCalled"
                        {
                            kisMyListUpdated = "MyListCalledisUpdated"
                            if containMylist == "yes"
                            {
                              UIView.transition(with: self.listView, duration: 1.0, options: .transitionCrossDissolve, animations: {self.listView.reloadSections([self.mylistSection], with: .none)}, completion: nil)
                            }
                            else
                            {
                              UIView.transition(with: self.listView, duration: 1.0, options: .transitionCrossDissolve, animations: {self.listView.reloadData()}, completion: nil)
                            }
                        }
                }
            }
            else
            {
                print("JSON ERROR")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                })
//                alertview.addAction(defaultAction)
//                self.present(alertview, animated: true, completion: nil)
            }
        }
    }

    // Service Call for getFetchRecentList
    func getFetchRecentList(withurl: String)
    {
        var parameters = [String:[String:AnyObject]]()
        parameters = ["fetchRecentlyWatched":["userId": userId as AnyObject, "userType": kUserType as AnyObject,"id":deviceId as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error, isDone) in
            if error ==  nil
            {
                let JSON = responseDict as! NSDictionary
                let arr = JSON["recentlyWatched"] as! NSArray
                var containsRecentList = String()
                self.recentList.removeAll()
                if arr.count == 0
                {
                    self.CarousalName.remove("Recently Watched")
                }
                for dict in arr as! [[String:Any]]
                {
                    let dicton = dict as NSDictionary
                 
                    self.recentList.append(dicton as! [String:Any])
                }
                DispatchQueue.main.async
                    {
                        if self.CarousalName.contains("Recently Watched")
                        {
                            self.recentSection = self.CarousalName.index(of: "Recently Watched")
                            containsRecentList = "yes"
                           
                        }
                        else
                        {
                            if  !(self.recentList.isEmpty)
                            {
                                if self.CarousalName.contains("SpotLight")
                                {
                                    if self.CarousalName.contains("My List")
                                    {
                                        self.CarousalName.insert("Recently Watched", at: 3)
                                        self.recentSection = 3
                                    }
                                    else
                                    {
                                        self.CarousalName.insert("Recently Watched", at: 2)
                                        self.recentSection = 2
                                    }
                                    
                                }
                                else
                                {
                                    if self.CarousalName.contains("My List")
                                    {
                                        self.CarousalName.insert("Recently Watched", at: 2)
                                        self.recentSection = 2
                                    }
                                    else
                                    {
                                        self.CarousalName.insert("Recently Watched", at: 1)
                                        self.recentSection = 1
                                    }
                                }
                                containsRecentList = "no"
                            }
                        }
                        if kisRecentlyUpdated == "RecentlyUpdated"
                        {
                            kisRecentlyUpdated = "RecentlyNotUpdated"
                            if containsRecentList == "yes"
                            {
                                UIView.transition(with: self.listView, duration: 1.0, options: .transitionCrossDissolve, animations: {self.listView.reloadSections([self.recentSection], with: .none)}, completion: nil)
                            }
                            else
                            {
                                UIView.transition(with: self.listView, duration: 1.0, options: .transitionCrossDissolve, animations: {self.listView.reloadData()}, completion: nil)
                            }
                            
                        }
                        
                }
            }
            else
            {
                print("JSON ERROR")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    func StoreDetails()
    {
        ApiManager.sharedManager.getDataWithJson(url:kStoreUrl)
        {
            (responseDict) in
            if responseDict != nil
            {
                let post = responseDict as! NSDictionary
                let data = post.value(forKey: "dataa") as! [[String:Any]]
                self.storeProdData = data
            }
            else
            {
                
            }
        }
    }
    
    func getFeatured()
    {
        ApiManager.sharedManager.getDataWithJson(url:kFeaturedUrl)
        {
            (responseDict) in
            if responseDict != nil
            {
                let post = responseDict as! [[String:Any]]
                self.FeaturedDict = post
                DispatchQueue.main.async
                    {
                        if self.CarousalName.contains("SpotLight")
                        {
                            self.listView.reloadData()
                        }
                        else
                        {
                            if  !(self.FeaturedDict.isEmpty)
                            {
                                self.CarousalName.insert("SpotLight", at: 1)
                            }
                             self.listView.reloadData()
                        }
                       
                }
            }
            else
            {
                
            }
        }
    }
    
    func getPhotos()
    {
        ApiManager.sharedManager.getDataWithJson(url:kgetPhoto)
        {
            (responseDict) in
            if responseDict != nil
            {
                let post = responseDict as! NSDictionary
                let dict = post.value(forKey: "photos") as! [[String:Any]]
                self.PhotoDict = dict
            }
            else
            {
                
            }
        }

    }
    
    func getDonatedata()
    {
        let parameters = ["gets3productDataFinal": ["bucket": "Donation.json"]]
        ApiManager.sharedManager.postDataWithJson(url: kDonateUrl, parameters: parameters as [String : [String : AnyObject]]){(responseDict, error, isDone) in
            if error ==  nil
            {
               let post = responseDict as! NSDictionary
               self.DonateDict = post.value(forKey: "dataa") as! [[String : Any]]
            }
            else
            {
                
            }
    }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func myListdata(mylistDict: NSDictionary)
    {
        getFetchMyList(withurl: kFetchMyListUrl)
//        myList.append(mylistDict as! [String : Any])
//        if myList.isEmpty
//        {
//            print("mylist is empty")
//        }
//        else
//        {
//            if CarousalName.contains("My List")
//            {
//                listView.reloadData()
//            }
//            else
//            {
//                if CarousalName.contains("SpotLight")
//                {
//                    CarousalName.insert("My List", at: 2)
//                }
//                else
//                {
//                    CarousalName.insert("My List", at: 1)
//                }
//                
//            }
//        }
//        listView.reloadData()
    }
    
    func removeListdata(id:String) {
        getFetchMyList(withurl: kFetchMyListUrl)
//        var i:Int = 0
//        for dict in myList
//        {
//            let dicton = dict["data"] as! NSDictionary
//            let assetid = dicton["id"] as! String
//            if assetid == id
//            {
//                myList.remove(at: i)
//                if myList.isEmpty
//                {
//                    CarousalName.remove("My List")
//                }
//            }
//            i=i+1
//        }
//        listView.reloadData()
    }
    
    func recentlyWatcheddata(recentlyWatchedDict:NSDictionary)
    {
        getFetchRecentList(withurl: kFetchRecentUrl)
//        if !(recentList.isEmpty)
//        {
//            if CarousalName.contains("Recently Watched")
//            {
//                listView.reloadData()
//            }
//            else
//            {
//                if CarousalName.contains("Spot Light")
//                {
//                    if CarousalName.contains("My List")
//                    {
//                        CarousalName.insert("Recently Watched", at: 3)
//                    }
//                    else
//                    {
//                        CarousalName.insert("Recently Watched", at: 2)
//                    }
//                }
//                else
//                {
//                    if CarousalName.contains("My List")
//                    {
//                        CarousalName.insert("Recently Watched", at: 2)
//                    }
//                    else
//                    {
//                        CarousalName.insert("Recently Watched", at: 1)
//                    }
//                }
//            }
//        }
//        listView.reloadData()
    }
}

//Extension Methods for myListDelegate
//extension MainViewController:myListdelegate
//{
//   
//}



