//
//  AppDelegate.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import KeychainAccess

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var isFirstTime:Bool?
    var deviceId,uuid,userId,status,code:String!
    var bundleID = "deviceId"
    var venderId: String!
    var uuidofDevice:String!
    var udid = UIDevice.current.identifierForVendor?.uuidString
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        gotoCode()
        return true
    }
    
    // This is the function called when application launchs//
    func gotoCode()
    {
        // Checking Internet connection
        if isConnectedToNetwork() == false
        {
            let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                                    UIAlertAction in
                                    exit(0)
                                })
                                alertview.addAction(defaultAction)
                                self.window?.rootViewController?.present(alertview, animated: true, completion: nil)

        }
        else
        {
            
            let activityView = ActivityView.init(frame:(self.window?.frame)!)
            self.window?.addSubview(activityView)
            
            venderId = getVenderId() /* To get the deviceID stored in Keychain access calling getVenderId function*/
            
            if venderId != ""
            {
                self.deviceId = venderId
            }
            else
            {
                setVenderId() /*If venderID returns empty string storing the deviceID by calling setVenderID function*/
            }
          venderId = "1234555"
            var parameters = [String:[String:AnyObject]]()
            parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject,"manufacturer":"Apple" as AnyObject,"device_name":"AppleTV" as AnyObject, "device_id":venderId! as AnyObject,"device_mac_address":"mac" as AnyObject,"brand_name":"Apple" as AnyObject,"host_name":"app" as AnyObject,"display_name":"apple" as AnyObject, "serial_number":"1234" as AnyObject,"version":"BlackBox Faith" as AnyObject,"platform":"TVOS" as AnyObject]]
           
            ApiManager.sharedManager.postDataWithJson(url: kActivationCodeUrl, parameters: parameters) { (responseDict, error,isDone)in
                if error == nil
                {
                    let dict = responseDict as! [String:String]
                    self.uuid = dict["uuid"]
                    self.deviceId = dict["device_id"]
                    self.getinfo() /* calling getaccountInfo to get status of device is paired or not*/
                }
                else
                {
                    print("error")
                }
            }
        }
    }
    
    // Account Info to get status of device is paired or not
    
    func getinfo()
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
            print(parameters)
        ApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
            
                let dict = Json as! NSDictionary
                print(dict)
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse == false
                {
                    let uservalue = ""
               
                    self.userId = uservalue
                    self.gotoMenu(userid: self.userId,deviceid: self.deviceId, uuid: self.uuid)
                }
                else
                {
                    let uservalue = self.getUserId()
                  
                    self.userId = uservalue
                    self.gotoMenu(userid: self.userId,deviceid: self.deviceId, uuid: self.uuid)
                }
            }
            else
            {
                
            }
    }
    }
    
    //setVenderId is the function to set the deviceID stored in KeyChain Access
    func setVenderId() {
        let keychain = Keychain(service: bundleID)
        do {
            try keychain.set(udid!, key: bundleID)
         
            venderId = getVenderId()
        }
        catch let error {
            print("Could not save data in Keychain : \(error)")
        }
      
    }
    
    
    // getVenderId is the function to get the deviceID stored in KeyChain Access it returns empty when there is no deviceID
    func getVenderId() -> String {
        
        let keychain = Keychain(service: bundleID)
        
        if try! keychain.get(bundleID) != nil
        {
            let token : String = try! keychain.get(bundleID)!
            
            return token
            
        }
        return ""
        
    }
    
    //getUserid
    func getUserId() -> String {
        
        let keychain = Keychain(service: "userId")
        
        if try! keychain.get("userId") != nil
        {
            let token : String = try! keychain.get("userId")!
            
            return token
        }
        return ""
        
    }
    
    // go to Main view controller
    func gotoMenu(userid:String,deviceid:String,uuid:String)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootView = storyBoard.instantiateViewController(withIdentifier: "Main") as! MainViewController
        rootView.deviceId = venderId
        rootView.userId = userid
        rootView.uuid = uuid
        let navigationController = UINavigationController(rootViewController: rootView)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

