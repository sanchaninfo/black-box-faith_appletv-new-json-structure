//
//  DDSSubscriptionViewController.swift
//  DameDashStudios
//
//  Created by Sanchan on 01/03/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class SubscriptionViewController: UIViewController {

    @IBOutlet weak var subLbl: UILabel!
    @IBOutlet weak var outView: UIView!
    @IBOutlet weak var ImgLogo: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        outView.layer.cornerRadius = 4.0
        outView.layer.borderWidth = 4.0
        outView.layer.borderColor = UIColor.init(red: 39/255, green: 39/255, blue: 39/255, alpha: 1.0).cgColor
        ImgLogo.layer.cornerRadius = ImgLogo.frame.height/2
        ImgLogo.clipsToBounds = true
        let str = "Your subscription has been expired,please upgrade\n subscription at \n \(kBaseUrl)"
        let attributedString = NSMutableAttributedString.init(string: str)
        self.subLbl.attributedText = attributedString
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

